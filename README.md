# Splooshy Bombs API

Explosions that fling debris!

This is largely a proof of concept. This mod provides a simple boom function for creating explosions, and a testing stick that can be used to cause explosions.

Warning: The API might be changed

This mod is not very performant with large explosions, use vaporize and a higher drop_percent for larger explosions to avoid excessive entities.

See splooshy_bombs.defaults (in init.lua) for parameters.