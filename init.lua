splooshy_bombs = {}
splooshy_bombs.NIL = {} -- placeholder for nil in params, do not modify
splooshy_bombs.defaults = {
  drop_percent = 75, -- chance of nodes dropping/vaporizing
  respect_protection = true, -- If true, leave protected areas untouched (untested)
  vaporize = true, -- If true, don't drop items (some drops from falling nodes may still occur)
  user = splooshy_bombs.NIL, -- Defaults to no user
  velocity = vector.new(0, 15, 0), -- Velocity bias for explosion
  damage = 0,
  damage_radius = splooshy_bombs.NIL -- radius for damage, defaults to same as radius
}

local random = PcgRandom(0)

-- see splooshy_bombs.defaults
-- Some arguments support using nil to use the default behaviour
function splooshy_bombs.boom(location, radius, options)
  if options == nil then
    options = {}
  end
  local params = {}
  for k, v in pairs(splooshy_bombs.defaults) do
    params[k] = options[k]
    if params[k] == nil then
      params[k] = v
    end
    if params[k] == splooshy_bombs.NIL then
      params[k] = nil
    end
  end
  if params.damage_radius == nil then
    params.damage_radius = radius
  end

  minetest.sound_play({
    name = "tnt_explode",
    gain = math.max(radius / 20, 1)
  }, {
    pos = location
  })

  if params.damage then
    for _, entity in pairs(minetest.get_objects_inside_radius(location, params.damage_radius)) do
      if entity:is_player() then
        local delta = entity:get_pos():subtract(location)
        entity:punch(params.user or entity, 0, {
          full_punch_interval = 0,
          damage_groups = {
            fleshy = params.damage * (1 - (delta:length() + 1) / (params.damage_radius + 1))
          }
        }, delta:normalize())
      end
    end
  end

  -- Break/fling blocks
  for y = location.y + radius, location.y - radius, -1 do -- Reversed to avoid issues with falling blocks
    for x = location.x - radius, location.x + radius do
      for z = location.z - radius, location.z + radius do
        local coord = vector.new(x, y, z)
        local delta = coord:subtract(location)
        local distance = delta:length()
        if distance <= radius and
            (not (params.user and params.respect_protection and minetest.is_protected(coord, params.user))) then
          if random:next(1, 100) <= params.drop_percent then
            if params.vaporize then
              minetest.set_node(coord, {
                name = "air"
              })
            else
              minetest.dig_node(coord)
            end
          else
            local ok, node = minetest.spawn_falling_node(coord)
            if ok then
              node:set_properties({
                collide_with_objects = false
              })
              if distance > 0 then
                -- This formula is very arbitrary
                node:set_velocity(delta:normalize():multiply(radius / 2):add(params.velocity):offset(
                    random:rand_normal_dist(-2, 2), random:rand_normal_dist(-2, 2), random:rand_normal_dist(-2, 2))
                    :multiply(1 - (distance / radius) ^ 2))

                -- Offset coords a bit to try and keep blocks from immediately sticking back
                node:set_pos(coord:subtract(delta:normalize():multiply(0.5)):offset(0, 0.5, 0))
              end
            end
          end
        end
      end
    end
  end
end

minetest.register_tool("splooshy_bombs_api:teststick", {
  description = "Splooshy Bombs test stick",
  visual = "wielditem",
  inventory_image = "splooshy_bombs_api_teststick.png",
  on_use = function(itemstack, user, pointed_thing)
    if pointed_thing and pointed_thing.under then
      splooshy_bombs.boom(pointed_thing.under, 10, {
        user = user
      })
    end
  end
})
